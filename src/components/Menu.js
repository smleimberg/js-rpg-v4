import React, { Component } from 'react'
import MenuButtonLink from '../containers/MenuButtonLink'
import OpenSubmenuLink from '../containers/OpenSubmenuLink'
import LoadSaveLink from '../containers/LoadSaveLink'
import DeleteSaveLink from '../containers/DeleteSaveLink'
import ChangeSettingLink from '../containers/ChangeSettingLink'
import PickupItemLink from '../containers/PickupItemLink'
import Item from './Item'
import { menuNames, menuButtons } from '../actions'
import '../styles/Menu.css';

class Menu extends Component {

  render(){
    var menu = false;
    var i,j;

    if(menuNames.NEW_GAME===this.props.game.current.currentMenu && this.props.game.current.gameStart===false){
      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>New Game</h1>
          </div>
          <div className="text">
            <p>The JavaScript Role Playing Game (JSRPG).</p>
          </div>
          <div className="submit">
            <MenuButtonLink buttonId={menuButtons.PLAY_FIRST_GAME}>Play</MenuButtonLink>
          </div>
        </div>
      )
    }

    else if(menuNames.MAIN_MENU === this.props.game.current.currentMenu){
      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>JSRPG</h1>
          </div>
          <div className="text">
            <p>Main Menu.</p>
          </div>
          <div className="options">
            <OpenSubmenuLink buttonId={menuNames.ITEMS_MENU}>Items</OpenSubmenuLink>
            <OpenSubmenuLink buttonId={menuNames.SAVES_MENU}>Saves</OpenSubmenuLink>
            <OpenSubmenuLink buttonId={menuNames.SETTINGS_MENU}>Settings</OpenSubmenuLink>
            <MenuButtonLink buttonId={menuButtons.CLOSE_MAIN_MENU}>&lt; Back</MenuButtonLink>
          </div>
        </div>
      )
    }

    else if(menuNames.SAVES_MENU === this.props.game.current.currentMenu ){
      var gameSaves = this.props.game.saves;
      var gameSavesHTML = [];
      var saveDate = false;
      var dateText = false;
      var dateOptions = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };
      for(var gameSaveID in gameSaves){
        var gameSave = gameSaves[gameSaveID];
        saveDate = new Date(gameSave.date);
        dateText = saveDate.toLocaleDateString("en-US", dateOptions);
        gameSavesHTML.push(
          <div className="save" key={gameSave.gameObject.gameID}>
            <div className="small-1 medium-2">
              <h2>Game {gameSave.gameObject.gameID}</h2>
              <p>{dateText}</p>
            </div>
            <div className="small-1 medium-2">
              <div className="save-btn-wrap small-2 medium-1">
                <LoadSaveLink buttonId={gameSave.gameObject.gameID.toString()}>Load</LoadSaveLink>
              </div>
              <div className="save-btn-wrap small-2 medium-1">
                <DeleteSaveLink buttonId={gameSave.gameObject.gameID.toString()}>Delete</DeleteSaveLink>
              </div>
            </div>
          </div>
        )
      }
      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>JSRPG Game Saves</h1>
          </div>
          <div className="text">
            <p>Manage your games saves.</p>
          </div>
          <div className="save-button">
            <MenuButtonLink buttonId={menuButtons.SAVE_THE_GAME}>Save Game {this.props.game.current.gameID}</MenuButtonLink>
          </div>
          <div className="game-saves">
            {gameSavesHTML}
          </div>
          <div className="back-button-wrap">
            <MenuButtonLink buttonId={menuButtons.PLAY_NEW_GAME}>New Game</MenuButtonLink>
            <OpenSubmenuLink buttonId={menuNames.MAIN_MENU}>&lt; Back</OpenSubmenuLink>
          </div>
        </div>
      )
    }

    else if(menuNames.SETTINGS_MENU === this.props.game.current.currentMenu ){
      var gameSettings = this.props.game.settings;
      var gameSettingsHTML = [];
      for(var gameSettingsID in gameSettings){
        var gameSetting = gameSettings[gameSettingsID];
        var gameSettingOptionsHTML = [];
        for(i=0;i<gameSetting.options.length;i++){
          var option = gameSetting.options[i];
          var settingKey = gameSetting.name+'_'+option.value;
          var optionIsSelected = gameSetting.value===option.value ? 'selected-option' : '';
          gameSettingOptionsHTML.push(
            <div key={settingKey} className="setting-btn-wrap small-1 medium-2">
              <ChangeSettingLink buttonId={option.value} className={optionIsSelected}>{option.text}</ChangeSettingLink>
            </div>
          );
        }
        gameSettingsHTML.push(
          <div className="setting" key={gameSetting.name}>
            <h2>{gameSetting.text}</h2>
            {gameSettingOptionsHTML}
          </div>
        )
      }
      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>JSRPG Game Settings</h1>
          </div>
          <div className="text">
            <p>Changing how you play the game.</p>
          </div>
          <div className="game-settings">
            {gameSettingsHTML}
          </div>
          <div className="back-button-wrap">
            <OpenSubmenuLink buttonId={menuNames.MAIN_MENU}>&lt; Back</OpenSubmenuLink>
          </div>
        </div>
      )
    }

    else if(menuNames.PICKUP_ITEMS_MENU === this.props.game.current.currentMenu ){
      var currentMap = this.props.game.current.maps[this.props.game.current.character.location.map];
      var itemsTile = currentMap.tiles[this.props.game.current.character.location.actionTile];
      var itemListHTML =[];
      var itemID = '';
      if(itemsTile.object.type==='chest'){
        for(i=0;i<itemsTile.object.items.length;i++){
          var itemKey = itemsTile.object.items[i];
          itemID = 'item-'+i;
          if(itemKey.indexOf('money_')===0){
            var ammount = parseInt(itemKey.replace('money_',''),10);
            var moneySize = 1;
            var moneySizes = [1,10,100,1000,10000,100000,1000000];
            for(j=0;j<moneySizes.length;j++){
              if(ammount>moneySizes[j]) moneySize = j+1;
            }
            var moneySizeClass = 'money-item size-'+moneySize;
            itemListHTML.push(
              <div key={itemID} className="game-item">
                <div className="item-wrap">
                  <Item item={item} />
                </div>
                <div className="name">{ammount} Coins</div>
                <div className="buttons">
                  <PickupItemLink buttonId={itemKey}>Pickup</PickupItemLink>
                </div>
              </div>
            );
          }else{
            var item = this.props.game.current.items[itemKey];
            var itemObject={'key':itemKey,'item':item};
            itemListHTML.push(
              <div key={itemKey} className="game-item">
                <div className="item-wrap">
                  <Item item={itemObject} />
                </div>
                <div className="name">{item.name}</div>
                <div className="buttons">
                  <PickupItemLink buttonId={itemKey}>Pickup</PickupItemLink>
                </div>
              </div>
            );

          }
        }
      }
      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>New Items!</h1>
          </div>
          <div className="text">
            <p>Grab what you need, leave the rest.</p>
          </div>
          <div className="game-items">
            {itemListHTML}
          </div>
          <div className="back-button-wrap">
            <MenuButtonLink buttonId={menuButtons.CLOSE_MAIN_MENU}>&lt; Back</MenuButtonLink>
          </div>
        </div>
      )
    }

    else if(menuNames.ITEMS_MENU === this.props.game.current.currentMenu ){
      var playerItems = this.props.game.current.character.inventory;
      var itemListHTML =[];
      var itemMenus = {
        "consumable":{
          "title": "Consumables",
          "menu": menuNames.CONSUMABLE_ITEMS_MENU
        },
        "tool":{
          "title": "Tools",
          "menu": menuNames.TOOL_ITEMS_MENU
        },
        "weapon":{
          "title": "Weapons",
          "menu": menuNames.WEAPON_ITEMS_MENU
        },
        "armor":{
          "title": "Armor",
          "menu":menuNames.ARMOR_ITEMS_MENU
        },
        "key":{
          "title":"Keys",
          "menu":menuNames.KEY_ITEMS_MENU
        }
      };
      for(var itemType in playerItems){
        if( Object.keys(playerItems[itemType]).length > 0 ){
          itemListHTML.push(<OpenSubmenuLink buttonId={itemMenus[itemType].menu}>{itemMenus[itemType].title}</OpenSubmenuLink>)
        }
      }

      menu = (
        <div id="main-menu">
          <div className="title">
            <h1>Items</h1>
          </div>
          <div className="text">
            <p>They do things.</p>
          </div>
          <div className="game-items">
            {itemListHTML}
          </div>
          <div className="back-button-wrap">
            <MenuButtonLink buttonId={menuButtons.CLOSE_MAIN_MENU}>&lt; Back</MenuButtonLink>
          </div>
        </div>
      )
    }

    return(
      <div id="menu">
        <div id="screen-wrap">
          <div id="screen">
            {menu}
          </div>
        </div>
      </div>
    )
  }
}

export default Menu
