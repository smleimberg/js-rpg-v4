#Item Notes

## Materials
### Other
### Materials Metals & Ore
Listed from worst to best
- Leather
- Bronze
- Iron
- Steel
- Platinum
- Mithral
- Adamantine
## Wood Types
Listed from worst to best
- Cedar
- Pine
- Oak
- Walnut
- Ash
- Hickory
## Orb Types
- Glass
- Quarts
- Sapphire
- Topaz
- Zircon
- Diamond

## Magic Types
### Elemental
- Wind
- Water
- Earth
- Fire
- Electric
- Light
- Shadow
### Life Energy
- Life
- Death

## Weapon Types
### Magic Weapons
- Wand (1h)
- Staff (2h)
- Orb (2h)
### Bladed
- Knife (1h)
- Katana (2h)
- Sword (1h)
- Broad Sword (2h)
- Hatchet (1h)
- Axe (1h)
- Battle Axe (2h)
### Stab
- Spear (2h)
- Rapier (1h)
- Dagger (1h)
### Blunt
- Hammer (1h)
- War Hammer (2h)
- Mace (1h)
- War Mace (2h)
- Staff (2h)
### Shields (1h)
- Buckler
- Round
- Oval
- Heater
- Kite
- Scutum
### Ranged (2h)
- Bow (arrows)
- Crossbow (bolts)

## Mythical Weapons
- Master Sword (Zelda)
- Elder Wand (Harry Potter)
- Sting (Lord Of The Rings)
- Gandalf's Staff (Lord Of The Rings)
- Keyblade (Kingdom Hearts)
- Buster Sword (Final Fantasy 7)
- Mjolnir (Thor's Hammer)
